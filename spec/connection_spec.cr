require "./spec_helper"
require "../src/gemini_server/connection"

PORT_STANDARD  = 1965
ADDR_LOCAL_IP4 = "127.0.0.1"
ADDR_LOCAL_IP6 = "::1"

include GeminiServer

def validate_ipv4(req)
  Connection.validate_request(req, PORT_STANDARD, ADDR_LOCAL_IP4, "localhost")
end

def validate_ipv6(req)
  Connection.validate_request(req, PORT_STANDARD, ADDR_LOCAL_IP6, "localhost")
end

macro test_invalid_request(title1, title2, req, reply_type)
  it {{title1}} do
    expect_raises(RequestError) do
      res = validate_ipv4({{req}})
    end
  end

  it {{title2}} do
    begin
      validate_ipv4({{req}})
    rescue e : RequestError
      e.reply.should be_a({{reply_type}})
    end
  end
end

describe Connection do
  # Valid requests

  it "allows request to localhost" do
    req = "gemini://localhost/\r\n"
    res = validate_ipv4(req)
    res.host.should eq("localhost")
  end

  it "allows request to document root" do
    req = "gemini://localhost/\r\n"
    res = validate_ipv4(req)
    res.path.should eq("/")
  end

  it "allows request to subdir" do
    req = "gemini://localhost/subdir\r\n"
    res = validate_ipv4(req)
    res.path.should eq("/subdir")
  end

  it "allows 1024 bytes request" do
    path = "x" * (1024 - "gemini://localhost/".size)
    req = "gemini://localhost/" + path + "\r\n"
    res = validate_ipv4(req)
    res.path.should eq("/" + path)
  end

  it "allows request with ipv4-address" do
    req = "gemini://127.0.0.1/\r\n"
    res = validate_ipv4(req)
    res.host.should eq("127.0.0.1")
  end

  it "allows request with ipv6-address" do
    req = "gemini://[::1]/\r\n"
    res = validate_ipv6(req)
    res.host.should eq("[::1]")
  end

  # Invalid requests

  test_invalid_request(
    "does not allow nil request",
    "returns correct reply on nil request",
    nil,
    Reply::None
  )

  test_invalid_request(
    "does not allow invalid end of request",
    "returns correct reply on invalid end of request",
    "gemini://localhost/",
    Reply::None
  )

  test_invalid_request(
    "does not allow too large request",
    "returns correct reply on too large request",
    "gemini://localhost/" + ("x" * (1025 - "gemini://localhost/".size)) + "\r\n",
    Reply::BadRequestError
  )

  # I don't know if we can test this. Crystal does not allow \uDCDC in strings
  # test_invalid_request(
  #   "does not allow invalid utf8 characters",
  #   "returns correct reply on invalid utf8 characters",
  #   "gemini://localhost/\uDCDC\r\n",
  #   Reply::BadRequestError
  # )

  test_invalid_request(
    "does not allow empty scheme",
    "returns correct reply on empty scheme",
    "://localhost/\r\n",
    Reply::BadRequestError
  )

  test_invalid_request(
    "does not allow invalid gemini scheme",
    "returns correct reply on invalid gemini scheme",
    "https://localhost/\r\n",
    Reply::ProxyRequestRefused
  )

  test_invalid_request(
    "does not allow another port than the one we're listening on",
    "returns correct reply on another port than the one we're listening on",
    "gemini://localhost:1234/\r\n",
    Reply::ProxyRequestRefused
  )

  test_invalid_request(
    "does not allow another host than configured",
    "returns correct reply on another host than configured",
    "gemini://example.org/\r\n",
    Reply::ProxyRequestRefused
  )
end
