require "./spec_helper"
require "../src/gemini_server/reply"

include GeminiServer

REDIRECT_URL = "gemini://localhost:1234/index.gmi"

macro spec_reply_code(class_type, code)
  it "{{class_type}} returns code {{code}}" do
    r = {{class_type}}.new
    r.code.should eq({{code}})
  end
end

macro spec_reply_code_with_meta(class_type, code, meta)
  it "{{class_type}} returns code {{code}}" do
    r = {{class_type}}.new({{meta}})
    r.code.should eq({{code}})
  end
end

macro spec_reply_meta(class_type, meta)
  it "{{class_type}} returns meta-data" do
    r = {{class_type}}.new({{meta}})
    r.meta.should eq({{meta}})
  end
end

macro spec_success_data
  Reply::SuccessData.new(
    "text/gemini",
    IO::Memory.new "file content"
  )
end

macro spec_success_file
  Reply::SuccessFile.new(
    "text/gemini",
    Path.new("/path/to/filename.gmi")
  )
end

describe Reply do
  # The following replies does not require meta-data

  spec_reply_code(Reply::Input, 10)
  spec_reply_code(Reply::InputHidden, 11)

  spec_reply_code(Reply::TemporaryError, 40)
  spec_reply_code(Reply::ServerError, 41)
  spec_reply_code(Reply::CgiError, 42)
  spec_reply_code(Reply::ProxyError, 43)
  spec_reply_code(Reply::SlowDown, 44)

  spec_reply_code(Reply::PermanentError, 50)
  spec_reply_code(Reply::NotFoundError, 51)
  spec_reply_code(Reply::GoneError, 52)
  spec_reply_code(Reply::ProxyRequestRefused, 53)
  spec_reply_code(Reply::BadRequestError, 59)

  spec_reply_code(Reply::None, 0)

  # The above replies can have meta-data too, although it's not required

  spec_reply_code_with_meta(Reply::BadRequestError, 59, "This request was not good :(")

  # The following replies requires meta-data

  spec_reply_code_with_meta(Reply::RedirectTemporary, 30, REDIRECT_URL)
  spec_reply_code_with_meta(Reply::RedirectPermanent, 31, REDIRECT_URL)

  spec_reply_meta(Reply::RedirectTemporary, REDIRECT_URL)
  spec_reply_meta(Reply::RedirectPermanent, REDIRECT_URL)

  # Success replies have files or filenames attached

  it "Reply::SuccessData returns code 20" do
    r = spec_success_data
    r.code.should eq(20)
  end

  it "Reply::SuccessFile returns code 20" do
    r = spec_success_file
    r.code.should eq(20)
  end

  it "Reply::SuccessData returns mime-type" do
    r = spec_success_data
    r.meta.should eq("text/gemini")
  end

  it "Reply::SuccessFile returns mime-type" do
    r = spec_success_file
    r.meta.should eq("text/gemini")
  end

  it "Reply::SuccessData returns data" do
    r = spec_success_data
    r.file.gets_to_end.should eq("file content")
  end

  it "Reply::SuccessFile returns filename" do
    r = spec_success_file
    r.filename.should eq(Path.new("/path/to/filename.gmi"))
  end
end
