bin/gemini-server: src/*.cr src/gemini_server/*.cr
	shards build

release: src/*.cr src/gemini_server/*.cr
	shards build --release
	strip bin/gemini-server

test:
	crystal spec

clean:
	rm -f bin/gemini-server

tag:
	git tag "v$(shell grep "^version: 0.1.0" shard.yml | cut -d " " -f 2)"
