#!/bin/sh
set -e

if [ -z "$2" ] ; then
    echo "Usage: gen_cert.sh CERT_DIR HOSTNAME [ALT_NAMES]"
    echo 'ALT_NAMES is optional, if not given it will be set to HOSTNAME'
    echo 'ALT_NAMES is in the format "DNS:example.com,DNS:myhost.test,IP:127.0.0.1"'
    exit 1
fi
out_dir="$1"
server_name="$2"
alt_names="$3"

key="${out_dir}/server.key"
cert="${out_dir}/server.cert"

if [ -f "$key" ] || [ -f "$cert" ] ; then
    echo "$key or $cert already exists, remove them if you want to re-generate them"
    exit 1
fi

mkdir -p "$out_dir"

if [ -z "$alt_names" ] ; then
    openssl req -new -subj "/CN=${server_name}" -x509 -newkey ec \
        -pkeyopt ec_paramgen_curve:prime256v1 -days 36500 -nodes \
        -out "$cert" -keyout "$key" \
        -addext "subjectAltName=DNS:${server_name}"
else
    openssl req -new -subj "/CN=${server_name}" -x509 -newkey ec \
        -pkeyopt ec_paramgen_curve:prime256v1 -days 36500 -nodes \
        -out "$cert" -keyout "$key" \
        -addext "subjectAltName=${alt_names}"
fi
