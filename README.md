# Entropic Gemini Server

Note: This software is experimental at this moment. Don't use in production.

This package can be used both as a standalone server and as a library to base
other applications on. To use as a library, add to your shard:
```yaml
dependencies:
  gemini-server:
    gitlab: entropic-software/gemini-server

```
Then use src/main.cr as template for how to initialize your application.

## Gemini protocol support

### No support
- Client certificates
- CGI

## Installation

Install [Crystal](https://crystal-lang.org).

Run `shards build` or `shards build --release` to build the binary.

Look at `install-example.sh` and ensure that it does what you expect.
Run it with `./install-example.sh`, or copy it, customize it and run the
customized one.

Generate a certificate:
`./gen-cert.sh /srv/gemini/etc your.host.name`

Or if using several hostnames:
`./gen-cert.sh /srv/gemini/etc hostname1 DNS:hostname1,DNS:hostname2`

Set permissions of the certificate and key:
```bash
chmod 600 /srv/gemini/etc/server.key
chown root:root /srv/gemini/etc/server.key
chown root:root /srv/gemini/etc/server.cert
```

Copy `gemini-server.yml.template` to `/srv/gemini/etc/gemini-server.yml` and
edit it, especially the `server_name` setting, it should be the same as
the hostname(s) in the certificate.

If using systemd, enable and start the server with
`systemctl enable gemini-server` and `systemctl start gemini-server`.

## Usage

Put gemtext documents in `/srv/gemini/public`. The file `index.gmi` will be
displayed to the client if they request a path to a directory.

If there is no `index.gmi` file in a directory, its contents will
be listed by the server, if configured (see below).

## Configuration

Example virtual host syntax:
```yaml
virtual_hosts:
  - server_name: gemini1.example.com alias1.example.com
    root: /path/to/docs1
  - server_name: gemini2.example.com
    root: /path/to/docs2
    enable_dir_listing: true
```

### virtual_hosts / server_name
Space separated list of hostnames and IP-addresses this server should listen to.
No default, must be set.

### virtual_hosts / root
Where to put your public Gemini documents.
If `chroot` is set, this is relative to the chroot dir. No default, must be set.

### virtual_hosts / enable_dir_listing
If true, show the contents of directories if no `index.gmi` exists in the
directory. Defaults to false if unset.

### max_clients
Max simultaneous clients. Defaults to 50 if unset.

### port
Defaults to 1965 if unset.

### user
Username of a non-root user. If unset, the server will not switch user.

### chroot
Set application root to this directory. After the server has started, it
cannot access files outside of this directory. See individual configuration
options for information whether they are affected by this setting.

### key
Path to a TLS private key. No default, must be set. Not relative to `chroot`.

### cert
Path to a TLS certificate. No default, must be set. Not relative to `chroot`.

### log_file
Defaults to logging to stderr if unset. Not relative to `chroot`.


## Author

Created by Tomas Åkesson <tomas@entropic.se>

See LICENSE for license details.

Project homepage: https://gitlab.com/entropic-software/gemini-server/
