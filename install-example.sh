#!/bin/sh
set -e

useradd -r -U gemini || true

install -m 755 -o root -g root -d /srv/gemini/bin
install -m 755 -o root -g root -d /srv/gemini/etc
install -m 755 -o root -g root -d /srv/gemini/public
install -m 750 -o root -g root -d /srv/gemini/log

install -m 755 -o root -g root bin/gemini-server /srv/gemini/bin
install -m 644 -o root -g root gemini-server.service /etc/systemd/system

echo "Install ok"
