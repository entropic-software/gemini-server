require "./config"
require "./reply"

module GeminiServer
  abstract class UriHandler
    getter vhosts
    getter document_root
    getter enable_dir_listing

    @vhosts : Array(String)
    @document_root : String
    @enable_dir_listing : Bool

    def initialize(vhost_config : GeminiServer::VirtualHostConfig)
      @vhosts = vhost_config.server_name.split(" ")
      @document_root = vhost_config.root
      @enable_dir_listing = vhost_config.enable_dir_listing
    end

    def matches_hostname?(uri : URI) : Bool
      @vhosts.includes?(URI.unwrap_ipv6(uri.host.as(String)))
    end

    abstract def can_handle?(uri : URI) : Bool

    abstract def handle(
      document_root : Path,
      uri : URI,
      config : GeminiServer::Config
    ) : GeminiServer::Reply::Base
  end
end
