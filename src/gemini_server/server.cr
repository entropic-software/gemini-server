require "mime"
require "openssl"
require "socket"
require "system/user"

require "./connection"
require "./uri_handler"

module GeminiServer
  @[Flags]
  enum Command
    ClientConnected
    ClientDisconnected
    GracefulQuit
    Quit
  end

  class Server
    def self.open_log(config)
      if !config.log_file.nil?
        Dir.mkdir_p(File.dirname(config.log_file.to_s))
        Log.setup(:info, Log::IOBackend.new(File.new(config.log_file.to_s, "a+")))
      end
    end

    def self.setup_mime_types
      begin
        File.open("/etc/mime.types") do |io|
          MIME.load_mime_database(io)
        end
      rescue e
        Log.error {
          "Failed to load mime-type database /etc/mime.types," +
            "using a default limited set of mime-types."
        }
      end
      MIME.register(".gmi", "text/gemini")
    end

    def self.start_server(config)
      begin
        server = TCPServer.new(config.port)
      rescue e
        Log.error { e }
        exit
      end
      Log.info { "Server started at #{config.port}" }
      server
    end

    def self.run(config : GeminiServer::Config, uri_handlers : Array(UriHandler))
      open_log(config)

      setup_mime_types

      server = start_server(config)

      ssl_context = get_ssl_context(config)

      drop_privs(config)

      command_channel = Channel(Command).new
      num_clients : Int32 = 0
      running : Bool = true

      Signal::INT.trap do
        Log.info { "SIGINT received. Quitting." }
        command_channel.send Command::Quit
      end

      Signal::QUIT.trap do
        Log.info { "SIGQUIT received. Quitting." }
        command_channel.send Command::Quit
      end

      Signal::TERM.trap do
        Log.info { "SIGTERM received. Waiting for clients to disconnect..." }
        command_channel.send Command::GracefulQuit
        while num_clients > 0
          Log.info { "Still waiting, #{num_clients} left..." }
          sleep 1
        end
        Log.info { "Quitting." }
        exit
      end

      Signal::HUP.trap do
        Log.info { "SIGHUP received. Waiting for clients to disconnect..." }
        command_channel.send Command::GracefulQuit
        while num_clients > 0
          Log.info { "Still waiting, #{num_clients} left..." }
          sleep 1
        end
        Log.info { "Quitting." }
        exit
      end

      spawn(name: "command") do
        loop do
          select
          when command = command_channel.receive
            if command == Command::Quit
              exit
            end
            running = false if command == Command::GracefulQuit
            num_clients += 1 if command == Command::ClientConnected
            num_clients -= 1 if command == Command::ClientDisconnected
          end
        end
      end

      loop do
        if !running
          Fiber.yield
          next
        elsif num_clients >= config.max_clients
          Log.info { "Max clients (#{config.max_clients}) reached" }
          sleep 1
          next
        else
          begin
            if socket = server.accept?
              spawn do
                command_channel.send Command::ClientConnected
                begin
                  GeminiServer::Connection.handle_connection(
                    socket,
                    ssl_context,
                    config,
                    uri_handlers
                  )
                  socket.close
                ensure
                  command_channel.send Command::ClientDisconnected
                end
              end
            else
              break
            end
          rescue e : Socket::Error
            Log.error { e }
            sleep 1
          end
        end
      end
    end

    def self.drop_privs(config)
      if !config.user.nil?
        user = System::User.find_by name: config.user.to_s
        uid = user.id.to_i
      end

      if !config.chroot.nil?
        Process.chroot(config.chroot.to_s)
        Log.info { "Chrooting to #{config.chroot}" }
      end

      if !config.user.nil? && !uid.nil?
        ures = LibC.setuid(uid)
        raise "Failed to swith to user: #{uid}, error #{Errno.value}" if ures != 0
        raise "Failed to swith to user: #{uid}" if LibC.getuid != uid
        Log.info { "Dropping privileges to #{config.user} (#{uid})" }
      end
      raise "Running as root is not allowed" if LibC.getuid == 0
    rescue e
      Log.error { e }
      exit
    end

    def self.get_ssl_context(config)
      ssl_context = OpenSSL::SSL::Context::Server.new
      # Verify mode for client:
      ssl_context.verify_mode = OpenSSL::SSL::VerifyMode::NONE
      ssl_context.private_key = config.key
      ssl_context.certificate_chain = config.cert
      ssl_context.add_options(
        OpenSSL::SSL::Options::ALL |
        OpenSSL::SSL::Options::NO_SSL_V2 |
        OpenSSL::SSL::Options::NO_SSL_V3 |
        OpenSSL::SSL::Options::NO_TLS_V1 |
        OpenSSL::SSL::Options::NO_TLS_V1_1
      )
      ssl_context
    end
  end
end
