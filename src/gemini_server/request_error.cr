require "./reply"

module GeminiServer
  class RequestError < Exception
    def initialize(reply : GeminiServer::Reply::Base)
      @reply = reply
    end

    def reply
      @reply
    end
  end
end
