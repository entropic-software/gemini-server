require "socket"
require "uri"

require "./uri_handler"
require "./config"
require "./request_error"
require "./reply"

module GeminiServer
  class Connection
    BUF_SIZE = 32768

    def self.validate_request(request : String?, server_port) : URI
      raise RequestError.new(Reply::None.new) if request.nil?
      raise RequestError.new(Reply::None.new) if !request.ends_with?("\r\n")

      request = request.rstrip

      raise RequestError.new(Reply::BadRequestError.new) if !(1..1024).includes?(request.size)
      raise RequestError.new(Reply::BadRequestError.new) if !request.valid_encoding?

      uri = URI.parse request

      raise RequestError.new(Reply::BadRequestError.new) if uri.scheme.nil?
      raise RequestError.new(Reply::ProxyRequestRefused.new) if uri.scheme != "gemini"

      if !uri.port.nil? && uri.port != server_port
        raise RequestError.new(Reply::ProxyRequestRefused.new)
      end

      return uri
    end

    def self.handle_request(
      server_port,
      server_address,
      request : String?,
      config,
      uri_handlers : Array(UriHandler)
    ) : GeminiServer::Reply::Base
      uri = validate_request(request, server_port)

      matched_any_hostname = false
      uri_handlers.each do |handler|
        # First check if the requested hostname matches this handler's
        # virtual hostname.
        if handler.matches_hostname?(uri)
          matched_any_hostname = true
        else
          next
        end

        # Then check if the handler wants to handle this url.
        # This can be for example an application matching paths dynamically.
        # File handler always returns true here.
        next if !handler.can_handle?(uri)

        return handler.handle(Path.new(handler.document_root), uri, config)
      end

      # If the requested hostname didn't match any of our configured hostnames,
      # return `ProxyRequestRefused`
      return Reply::ProxyRequestRefused.new if !matched_any_hostname

      return GeminiServer::Reply::NotFoundError.new
    rescue e : GeminiServer::RequestError
      return e.reply
    rescue e
      Log.error { e }
      return GeminiServer::Reply::TemporaryError.new
    end

    # A copy of IO.copy, but with `Fiber.yield` added
    def self.my_copy(src, dst) : Int64
      buffer = uninitialized UInt8[BUF_SIZE]
      count = 0_i64
      while (len = src.read(buffer.to_slice).to_i32) > 0
        dst.write buffer.to_slice[0, len]
        count &+= len
        Fiber.yield
      end
      count
    end

    def self.handle_connection(
      tcp_socket : TCPSocket,
      ssl_context : OpenSSL::SSL::Context,
      config : GeminiServer::Config,
      uri_handlers : Array(UriHandler)
    )
      request_start = Time.local
      ssl_socket = OpenSSL::SSL::Socket::Server.new(tcp_socket, ssl_context)
      client_address = tcp_socket.remote_address.address

      request : String? = ssl_socket.gets(1027)
      reply : GeminiServer::Reply::Base = handle_request(
        tcp_socket.local_address.port,
        tcp_socket.local_address.address,
        request,
        config,
        uri_handlers
      )

      document_size = 0

      if reply.is_a? GeminiServer::Reply::SuccessFile
        ssl_socket.print reply.header
        file : IO = File.open(reply.filename)
        document_size = my_copy file, ssl_socket
        file.close
      elsif reply.is_a? GeminiServer::Reply::SuccessData
        ssl_socket.print reply.header
        document_size = my_copy reply.file, ssl_socket
        reply.file.close
      elsif !reply.is_a? GeminiServer::Reply::None
        ssl_socket.print reply.header
        document_size = reply.header.size
      else
        # Don't even print header
      end

      log_request(
        tcp_socket.remote_address,
        request,
        reply,
        request_start,
        document_size
      )

      ssl_socket.close
    rescue e : OpenSSL::SSL::Error | IO::Error
      Log.error { e }
    end

    def self.log_request(
      client_address : Socket::IPAddress,
      request : String?,
      reply : GeminiServer::Reply::Base,
      request_start : Time,
      document_size
    )
      now = Time.local
      elapsed = now - request_start
      Log.info {
        "Request from: #{client_address.address}, code: #{reply.code}, " +
          "size: #{document_size}, time: #{elapsed.milliseconds} ms, " +
          "target: #{request.to_s.strip}"
      }
    end
  end
end
