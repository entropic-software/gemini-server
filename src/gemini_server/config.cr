require "yaml"

module GeminiServer
  class VirtualHostConfig
    include YAML::Serializable
    include YAML::Serializable::Strict

    property server_name : String
    property root : String
    property enable_dir_listing : Bool = false
  end

  class Config
    include YAML::Serializable
    include YAML::Serializable::Strict

    property virtual_hosts : Array(VirtualHostConfig)
    property max_clients : Int32 = 50
    property port : Int32 = 1965
    property key : String
    property cert : String
    property chroot : String? = nil
    property user : String? = nil
    property log_file : String? = nil
  end
end
