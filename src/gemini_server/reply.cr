macro reply_class(class_type, code, meta)
  class {{class_type}} < Base
    def initialize(meta = {{meta}})
      super
    end

    def code : Int32
      {{code}}
    end
  end
end

module GeminiServer
  module Reply
    abstract class Base
      abstract def code : Int32
      abstract def meta : String

      def initialize(meta : String)
        @meta = meta
      end

      def header : String
        "#{self.code} #{self.meta}\r\n"
      end

      def meta
        @meta
      end
    end

    reply_class(Input, 10, "Enter query")
    reply_class(InputHidden, 11, "Enter query")

    abstract class Success < Base
      def code : Int32
        20
      end
    end

    class SuccessData < Success
      def initialize(meta : String, file : IO)
        @meta = meta
        @file = file
      end

      def file : IO
        @file
      end
    end

    class SuccessFile < Success
      def initialize(meta : String, filename : Path)
        @meta = meta
        @filename = filename
      end

      def filename : Path
        @filename
      end
    end

    class RedirectTemporary < Base
      def code : Int32
        30
      end
    end

    class RedirectPermanent < Base
      def code : Int32
        31
      end
    end

    reply_class(TemporaryError, 40, "Temporary failure")
    reply_class(ServerError, 41, "Server unavailable")
    reply_class(CgiError, 42, "CGI Error")
    reply_class(ProxyError, 43, "Proxy Error")
    reply_class(SlowDown, 44, "Slow down")

    reply_class(PermanentError, 50, "Permanent failure")
    reply_class(NotFoundError, 51, "Not found")
    reply_class(GoneError, 52, "Gone")
    reply_class(ProxyRequestRefused, 53, "Proxy Request Refused")
    reply_class(BadRequestError, 59, "Bad request")

    reply_class(None, 0, "")
  end
end
