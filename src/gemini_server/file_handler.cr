require "./uri_handler"

module GeminiServer
  class FileHandler < UriHandler
    DIR_ICON  = "📁"
    FILE_ICON = "📄"

    def file_listing_line(icon : String, rel_path, filename : String) : String
      link : String = URI.encode_path((rel_path / filename).to_s)
      "=> #{link} #{icon} #{filename}"
    end

    def gen_directory_index(path_to_dir : Path, rel_path : Path) : IO
      dir = Dir.new(path_to_dir)
      dirs, files = dir.each.reject { |child|
        child.starts_with?(".")
      }.to_a.sort.partition { |child|
        File.directory?(path_to_dir / child)
      }
      dir.close

      entries = dirs.map { |child| file_listing_line(DIR_ICON, rel_path, child) } +
                files.map { |child| file_listing_line(FILE_ICON, rel_path, child) }

      parent : Path = rel_path.parent

      IO::Memory.new "# #{rel_path}\n" +
                     "=> #{parent} #{DIR_ICON} ..\n" +
                     entries.join("\n")
    end

    def can_handle?(uri : URI) : Bool
      true
    end

    def handle(
      document_root : Path,
      uri : URI,
      config : GeminiServer::Config
    ) : GeminiServer::Reply::Base
      uri_path : String = URI.decode(uri.path)

      requested_full_path : Path = (document_root / uri_path).normalize
      if !requested_full_path.to_s.starts_with?(document_root.to_s)
        return GeminiServer::Reply::NotFoundError.new
      end

      begin
        file : Path = if File.directory?(requested_full_path)
          index = requested_full_path / "index.gmi"
          if File.exists?(index)
            index
          else
            requested_full_path
          end
        else
          requested_full_path
        end
      rescue e : File::Error
        Log.error { e }
        return GeminiServer::Reply::NotFoundError.new
      end

      if File.exists?(file) && !File.readable?(file)
        Log.error { "Permission denied: #{file}" }
        return GeminiServer::Reply::NotFoundError.new
      end

      # Show directory index if a directory was requested, and enabled in config
      if File.directory?(file)
        if @enable_dir_listing
          return GeminiServer::Reply::SuccessData.new(
            "text/gemini",
            gen_directory_index(file, Path.new(uri_path))
          )
        else
          Log.error { "Directory access denied: #{file}" }
          return GeminiServer::Reply::NotFoundError.new
        end
      end

      if !File.exists?(file)
        Log.error { "Not found: #{file}" }
        return GeminiServer::Reply::NotFoundError.new
      end

      return GeminiServer::Reply::SuccessFile.new(
        MIME.from_filename(file, "application/octet-stream"),
        file
      )
    end
  end
end
