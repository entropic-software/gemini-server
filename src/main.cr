require "log"

require "./gemini-server"

module GeminiServer
  if ARGV.size < 1
    Log.error { "Needs a (full path to a) configuration file as argument" }
    exit
  end

  config_file = Path.new(ARGV[0])
  begin
    config = GeminiServer::Config.from_yaml(
      File.read(config_file)
    )
  rescue e
    Log.error { "Failed to read configuration file. " + e.to_s }
    exit
  end

  # Note that handlers are tried in the order they are added here,
  # so if FileHandler should be fallback, add it last.
  uri_handlers = [] of GeminiServer::UriHandler
  config.virtual_hosts.each do |vhost_config|
    uri_handlers << GeminiServer::FileHandler.new(vhost_config)
  end

  begin
    GeminiServer::Server.run(config, uri_handlers)
  rescue e
    Log.error { e }
    exit
  end
end
