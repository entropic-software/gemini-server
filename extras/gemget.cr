# Copyright (c) 2023 Tomas Åkesson <tomas@entropic.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

require "openssl"
require "option_parser"
require "socket"

REDIRECT_LIMIT =    5
BUF_SIZE       = 8192

def die(msg)
  STDERR.puts msg.to_s
  exit 1
end

Signal::INT.trap do
  exit
end
Signal::TERM.trap do
  exit
end

def handle_data(ssl_socket : OpenSSL::SSL::Socket::Client, outfile : String?)
  buf : Bytes = Bytes.new(BUF_SIZE)

  # TODO: dry!
  if !outfile.nil?
    File.open(outfile, "w") do |file|
      loop do
        bytes_read = ssl_socket.read(buf)
        break if bytes_read == 0
        file.write(buf[0, bytes_read])
        Fiber.yield
      end
    end
  else
    loop do
      bytes_read = ssl_socket.read(buf)
      break if bytes_read == 0
      STDOUT.write(buf[0, bytes_read])
      Fiber.yield
    end
  end
end

def show_tls_info(ssl_socket : OpenSSL::SSL::Socket)
  puts "Subject:"
  ssl_socket.peer_certificate.subject.to_a.each do |e|
    puts "  #{e.first}: #{e.last}"
  end

  alg = ssl_socket.peer_certificate.signature_algorithm
  puts "Algorithm: " + alg
  puts "Extensions:"
  ssl_socket.peer_certificate.extensions.each do |e|
    puts "  #{e.oid}: #{e.value}"
  end
end

def request(
  opt_uri : String,
  redirects : Int32,
  verbose : Bool,
  outfile : String?,
  tls_info : Bool
)
  die "Too many redirects" if redirects > REDIRECT_LIMIT

  opt_uri = "gemini://#{opt_uri}" unless /^gemini:\/\// =~ opt_uri
  uri : URI = URI.parse opt_uri
  die "Invalid hostname" if uri.host.nil?

  host : String = URI.unwrap_ipv6(uri.host.as(String))
  port : Int32 = if uri.port.nil?
    1965
  else
    uri.port.as(Int32)
  end

  socket = TCPSocket.new(host, port)
  ssl_context = OpenSSL::SSL::Context::Client.insecure
  ssl_socket = OpenSSL::SSL::Socket::Client.new(socket, ssl_context)

  if tls_info
    show_tls_info(ssl_socket)
    return
  end

  STDERR.puts "> #{opt_uri}\n" if verbose
  ssl_socket.print "#{opt_uri}\r\n"
  ssl_socket.flush

  header = ssl_socket.gets
  die "Invalid reply from server" if header.nil?

  header_matches = /^([0-9][0-9]) (.*)$/.match(header)
  die "Invalid reply from server" if header_matches.nil?
  code : Int32 = header_matches[1].to_i
  meta : String = header_matches[2]

  STDERR.puts "< #{header}\n" if verbose

  case code
  when 20
    handle_data(ssl_socket, outfile)
  when 30, 31
    # TODO: add hostname if relative?
    request(meta, redirects + 1, verbose, outfile, tls_info)
  when (40..59)
    die meta
  when (60..69)
    die "TODO: handle client certificate. Error: #{code} #{meta}"
  else
    die meta
  end
rescue e
  die e
end

opt_tls_info : Bool = false
opt_verbose : Bool = false
opt_outfile : String? = nil
opt_url : String = ""
OptionParser.parse do |parser|
  parser.banner = "Usage: gemget [options...] <url>"
  parser.on(
    "-v",
    "--verbose",
    "Display status messages to STDERR"
  ) {
    opt_verbose = true
  }
  parser.on(
    "-t",
    "--tls-info",
    "Display the server's TLS certificate to STDOUT and quit (don't get data)"
  ) {
    opt_tls_info = true
  }
  parser.on(
    "-o FILENAME",
    "--output=FILENAME",
    "Write to file instead of stdout"
  ) { |arg|
    opt_outfile = arg
  }
  parser.on("-h", "--help", "Show this help") do
    STDERR.puts parser
    exit
  end
  parser.unknown_args { |args, _|
    die parser if args.size < 1
    opt_url = args[0]
  }
  parser.invalid_option do |flag|
    die parser
  end
end

request(opt_url, 0, opt_verbose, opt_outfile, opt_tls_info)
